# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="powerline"

# Theme powerline setting
# POWERLINE_HIDE_HOST_NAME="true"
# POWERLINE_HIDE_USER_NAME="true"
POWERLINE_DETECT_SSH="true"
POWERLINE_RIGHT_A="exit-status"
POWERLINE_RIGHT_B="none"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to disable command auto-correction.
# DISABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git osx tmux sudo ruby node docker docker-compose autojump kubectl)

# include system profile to avoid path missing (such as /snap/bin)
source /etc/profile

# shellcheck source=/dev/null
source "$ZSH/oh-my-zsh.sh"

# User configuration

# export PATH="/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin"
# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

if [ -d '/opt/homebrew/bin' ]; then
  export PATH="/opt/homebrew/bin:$PATH"
fi

# locale
if [ "X$(uname)" = "XDarwin" ]; then
  export LC_ALL=en_US.UTF-8
  export LANG=en_US.UTF-8
fi

export EDITOR=vi
export VISUAL="$EDITOR"

export MINICOM='-w -C /tmp/minicom.log'

# github
if [ -f "$HOME/.github-token" ]; then
  tmp=$(cat "$HOME/.github-token")
  export GITHUB_TOKEN="$tmp"
fi

# aliases
if [ -f "$HOME/.aliases" ]; then
  # shellcheck source=/dev/null
  source "$HOME/.aliases"
fi

# add ssh key to keychain
if [ "X$SSH_AUTH_SOCK" != "X" ]; then
  ssh-add -K &>/dev/null
fi

# rbenv
if [ -d "$HOME/.rbenv" ]; then
  if [ "X$(uname)" != "XDarwin" ]; then
    export PATH="$HOME/.rbenv/bin:$PATH"
  fi
  eval "$(rbenv init -)"
fi

# goenv
if [ -d "$HOME/.goenv" ]; then
  [ ! -d "$HOME/.go" ] && mkdir -p .go
  export GOPATH="$HOME/.go"
  export GOENV_ROOT="$HOME/.goenv"
  export PATH="$GOPATH/bin:$GOENV_ROOT/bin:$PATH"
  eval "$(goenv init -)"
fi

# speed up brew setting
SPEED_BREW=.speed-up-brew
if [ "X$(uname)" = "XDarwin" ]; then
  export PATH="/usr/local/sbin:$PATH"
  if [ -f "$HOME/$SPEED_BREW" ]; then
    tmp=$(cat "$HOME/$SPEED_BREW")
    export PATH="${tmp}$PATH"
  else
    # path adjust for gnu tools, ori name without prefix-g
    for ite in coreutils gnu-sed grep findutils gnu-tar
    do
      if brew list 2>&1|grep -qsw "$ite"; then
        printf "%s/libexec/gnubin:" "$(brew --prefix $ite)" >> "$HOME/$SPEED_BREW"
      fi
    done
    tmp=$(cat "$HOME/$SPEED_BREW")
    export PATH="${tmp}$PATH"
  fi
fi

# cabal init
if [ -d "$HOME/.cabal" ]; then
  export PATH="$HOME/.cabal/bin:$PATH"
fi

# miniconda init
if [ -d "$HOME/.miniconda" ]; then
  export PATH="$HOME/.miniconda/bin:$PATH"
fi

# speed up powerline status setting
SPEED_PLST=.speed-up-plst
if [ -f "$HOME/$SPEED_PLST" ]; then
  tmp=$(cat "$HOME/$SPEED_PLST")
  export _POWERLINE_BIND_PATH="$tmp"
else
  if pip list|grep -sw powerline-status &>/dev/null; then
    printf "%s/powerline/bindings" "$(pip show powerline-status|grep -E ^Location:|awk '{print $2}')" > "$HOME/$SPEED_PLST"
    tmp=$(cat "$HOME/$SPEED_PLST")
    export _POWERLINE_BIND_PATH="$tmp"
  fi
fi

# color for list, base on brew in osx
if ls -G -d . &>/dev/null; then
  alias ls='ls -F --show-control-chars --color=auto'
fi

if which gdircolors &>/dev/null; then
  tmp=$(gdircolors -b "$HOME/.dir_colors")
  eval "$tmp"
fi

# nvm init
if [ -d "$HOME/.nvm" ]; then
  export NVM_DIR="$HOME/.nvm"
  if [ -s "$NVM_DIR/nvm.sh" ]; then
    # shellcheck source=/dev/null
    source "$NVM_DIR/nvm.sh"
  fi
fi

# hub wrap
if which hub &>/dev/null; then
  eval "$(hub alias -s)"
fi

# assh wrap
if which assh &>/dev/null; then
  alias ssh="assh wrapper ssh"
  if ! head -n1 "$HOME/.ssh/config"|grep -qs assh; then
    assh config build > "$HOME/.ssh/config"
  fi
fi

# iterm2
if [ -e "$HOME/.iterm2_shell_integration.zsh" ]; then
  # shellcheck source=/dev/null
  source "$HOME/.iterm2_shell_integration.zsh"
fi

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
